package main

import (
	"log"
	"os"

	"github.com/BurntSushi/toml"
	"inet.af/tcpproxy"
)

var handler tcpproxy.Proxy

var configFilename = "schuifkoppel.toml"

func main() {
	f, err := os.Open(configFilename)
	if err != nil {
		log.Fatal(err)
	}

	dec := toml.NewDecoder(f)
	if _, err := dec.Decode(&Configuration); err != nil {
		log.Fatal(err)
	}

	f.Close()

	/*
		Add handlers from configuration
	*/

	var whereWeAreListening = map[string]bool{}

	for _, d := range Configuration.Domain {
		log.Printf("%#v", d)
		handler.AddSNIRoute(d.Listen, d.Host, GetGeminiProxy(d.Address, d.Cert, d.Key))

		whereWeAreListening[d.Listen] = true
	}

	/*
		Add fallbacks for every port we are listening on
	*/

	for listening, _ := range whereWeAreListening {
		log.Println(listening)
		handler.AddRoute(listening, DefaultHandler{})
	}

	log.Fatal(handler.Run())
}

var Configuration ConfigStr

type ConfigStr struct {
	Domain []struct {
		Listen  string
		Host    string
		Address string
		Cert    string
		Key     string
	}
}
