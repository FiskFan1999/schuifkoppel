package main

import (
	"crypto/tls"
	"fmt"
	"log"
	"net"
	"strings"
	"time"

	"inet.af/tcpproxy"
)

const ErrorDeadline = time.Second * 5

func GetGeminiProxy(addr string, certPath, keyPath string) (p *tcpproxy.DialProxy) {
	p = new(tcpproxy.DialProxy)
	var certPair tls.Certificate
	var err error
	p.Addr = addr
	p.KeepAlivePeriod = -1 // no keepalive for gemini
	p.DialTimeout = time.Second * 5
	// p.DialContext = nil
	if certPath != "" {
		certPair, err = tls.LoadX509KeyPair(certPath, keyPath)
		if err != nil {
			log.Fatal(err)
		}
		p.OnDialError = func(src net.Conn, dstDialErr error) {
			tlsConn := tls.Server(src, &tls.Config{
				Certificates: []tls.Certificate{certPair},
			})
			defer tlsConn.Close()

			tlsConn.SetDeadline(time.Now().Add(ErrorDeadline))

			fmt.Fprintf(tlsConn, "43 %s\r\n", GetTextFromDialErr(dstDialErr))
		}
	} else {
		/*
			Since we do not have the key,
			we can only close the
			connection right away if there
			is an error.
		*/
		p.OnDialError = func(src net.Conn, dstDialErr error) {
			src.Close()
		}
	}

	return
}

func GetTextFromDialErr(err error) string {
	if strings.HasSuffix(err.Error(), "connection refused") {
		return "Bad gateway"
	}
	if strings.HasSuffix(err.Error(), "i/o timeout") {
		return "Gateway timeout"
	}

	return err.Error()
}
