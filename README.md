# schuifkoppel

schuifkoppel (dutch for "shove coupler") is a gemini reverse-proxy that uses SNI to differentiate hostnames. As the gemini specification states, clients MUST use server-name-indication, so this is a valid way to serve virtual hosts. Note that schuifkoppel is a stand-alone application, not a go submodule to be bundled in the binary of the daemons it is serving.

Once the hostname is known via SNI, schuifkoppel acts as a plaintext TCP proxy between the client and daemon (who complete the TLS handshake). This is required because unlike HTTP gemini uses client certificates (which the operator does not have the secret certificate for) for authentication.

It is possible to configure gemini virtual-hosting using NGINX. However, schuifkoppel may be preferrable as it can provide a gemini response in the case of failures (such as "Bad Gateway"). However, it is not always possible to send a response if we do not have the certificate and secret of the daemon we are reverse-proxying to (such as if the user specifies a hostname that we do not serve–it is also not required to provide the reverse-proxied daemon certificates).

# configuration example

```toml

[[domain]]
listen=":1965"
host="example.net"
address="localhost:1966"
cert="/path/to/certificate" # cert and key are optional
key="/path/to/key"

[[domain]]
listen=":1965"
host="alternate.example.net"
address="localhost:1967"
cert="/path/to/second/certificate"
key="/path/to/second/key"

```

# copyright

Copyright (C) 2023 William Rehwinkel

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
