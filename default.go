package main

import (
	"net"

	"inet.af/tcpproxy"
)

type DefaultHandler struct {
}

func (d DefaultHandler) HandleConn(c net.Conn) {
	/*
		Can't write anything because we
		don't have a certificate that
		matches whatever domain the
		client was asking for, and we
		can't send any plaintext message
		at all. So just close.
	*/
	c.Close()
}

// test that DefaultHandler satisfies
// tcpproxy.Target interface
var _ []tcpproxy.Target = []tcpproxy.Target{
	DefaultHandler{},
}
